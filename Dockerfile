FROM php:7.0-apache as testdocker

ENV APPSTAGE test
EXPOSE 80
WORKDIR /var/www/html

COPY src /var/www/html